var ctx = document.getElementById("myChart");
ctx.height = 250;
var options = {
    scales: {
        yAxes: [{
            ticks: {
                fontColor: "white",
                fontSize: 14,
                stepSize: 2,
                max: 8,
                beginAtZero: true
            },
            scaleLabel: {
                display: true,
                labelString: "pH Level",
                fontColor: "white",
                fontSize: 20
            },
            gridLines: {
                color: "white"
            }
        }],
        xAxes: [{
            ticks: {
                fontColor: "white",
                fontSize: 14,
                stepSize: 1,
                beginAtZero: true
            },
            gridLines: {
                display: false
            }
        }]
    },
    animation: {
        duration: 3000,
        onProgress: function(animation) {
            myChart.value = animation.animationObject.currentStep / animation.animationObject.numSteps;
        },
        onComplete: function () {
            // render the value of the chart above the bar
            var ctx = this.chart.ctx;
            ctx.font = (14, 'normal', Chart.defaults.global.defaultFontFamily);
            ctx.fillStyle = "white"
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            this.data.datasets.forEach(function (dataset) {
                for (var i = 0; i < dataset.data.length; i++) {
                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                    ctx.fillText(dataset.data[i], model.x, model.y - 5);
                }
            });
        }
    },
    legend: {
        display: false,
        labels: {
            fontColor: 'white'
        }
    }
}

var data = {
    type: 'bar',
    data: {
        labels: ["Water", "TechnoRoasted Coffee", "Traditional Coffee"],
        datasets: [{
            label: '',
            data: [7, 6.1, 5.2],
            backgroundColor: [
                'rgba(54, 162, 235, 1.0)',
                'rgba(255, 99, 132, 1.0)',
                'rgb(103, 133, 80)'
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(255,99,132,1)',
                'rgb(103, 133, 80)'
            ],
            borderWidth: 1
        }]
    },
    options: options
}

var myChart = new Chart(ctx, data);

var inView = false;

function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemTop <= docViewBottom) && (elemBottom >= docViewTop));
}

$(window).scroll(function() {
    if (isScrolledIntoView('#myChart')) {
        if (inView) { return; }
        inView = true;
        new Chart(ctx, data);
    } else {
        inView = false;  
    }
});